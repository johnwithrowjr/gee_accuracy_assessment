import ee
ee.Initialize()
import matplotlib.pyplot as plt

def getHistogram(imgX):
    return ee.Dictionary(ee.Image(imgX).reduceRegion(reducer=ee.Reducer.histogram(), bestEffort=True).get("b1"))

def getFixedHistogram(imgX,xMin,xMax,steps):
    x = ee.Image(imgX).reduceRegion(reducer=ee.Reducer.fixedHistogram(xMin,xMax,steps), bestEffort=True).get("b1").getInfo()
    y = []
    for i in range(len(x)):
        y.append(x[i][1])
    return y

def getAccuracy(tp,fp,tn,fn):
    NA = -1
    predp = tp+fp
    predn = tn+fn
    totn = tn+fp
    totp = tp+fn
    n = totn+totp
    if (totp > 0):
        tpr = float(tp)/float(totp)
        fnr = float(fn)/float(totp)
    else:
        tpr = NA
        fnr = NA
    if (totn > 0):
        tnr = float(tn)/float(totn)
        fpr = float(fp)/float(totn)
    else:
        tnr = NA
        fpr = NA
    if (predp > 0):
        ppv = float(tp)/float(predp)
        fdr = float(fp)/float(predp)
    else:
        ppv = NA
        fdr = NA
    if (predn > 0):
        fmr = float(fn)/float(predn)
        npv = float(tn)/float(predn)
    else:
        fmr = NA
        npv = NA
    if ((totp > 0) and (n > 0)):
        prev = float(totp)/float(n)
    else:
        prev = NA
    if (n > 0):
        acc = float(tp+tn)/float(n)
    else:
        acc = NA
    if ((tpr!=NA) and (fpr!=NA) and (fpr>0)):
        lrp = float(tpr)/float(fpr)
    else:
        lrp = NA
    if ((fnr!=NA) and (tnr!=NA) and (tnr>0)):
        lrn = float(fnr)/float(tnr)
    else:
        lrn = NA
    if ((lrp!=NA) and (lrn!=NA) and (lrn>0)):
        dor = float(lrp)/float(lrn)
    else:
        dor = NA
    if ((tpr!=NA) and (ppv!=NA) and (tpr>0) and (ppv>0)):
        f1 = 2.0/float(1/tpr+1/ppv)
    else:
        f1 = NA
    return {'Total_All':n,'Total_Positives':totp,'Total_Negatives':totn,'Predicted_Positives':predp,'Predicted_Negatives':predn,
            'True_Positives':tp,'False_Positives':fp,'True_Negatives':tn,'False_Negatives':fn,'True_Positive_Rate':tpr,
            'True_Negative_Rate':tnr,'False_Positive_Rate':fpr,'False_Negative_Rate':fnr,'Positive_Predictive_Value':ppv,
            'False_Discovery_Rate':fdr,'False_Omission_Rate':fmr,'Negative_Predictive_Value':npv,'Prevalence':prev,
            'Accuracy':acc,'Positive_Likelihood_Ratio':lrp,'Negative_Likelihood_Ratio':lrn,'Diagnostic_Odds_Ratio':dor,
            'Sensitivity':tpr,'Specificity':tnr,'ROC_Pair':[fpr,tpr],'F1_Score':f1}

def getAccuracyFromSingleThreshold(xMin,width,Xa,hist0,hist1,accstat,verbose=True):
    Na = int((Xa-xMin)/width)
    fp = sum(hist0[Na:])
    tp = sum(hist1[Na:])
    fn = sum(hist1[:Na])
    tn = sum(hist0[:Na])
    accObj = getAccuracy(tp,fp,tn,fn)
    if (verbose):
        if (accstat != 'ROC_Pair'):
            #print "        Xa    predp       tp       fp    predn       tn       fn            stat"
            #print "---------- -------- -------- -------- -------- -------- -------- ---------------"
            print "%9.6f %8d %8d %8d %8d %8d %8d %15.13f" % (Xa,(tp+fp),tp,fp,(tn+fn),tn,fn,accObj[accstat])
        else:
            #print "        Xa    predp       tp       fp    predn       tn       fn           stat1           stat2"
            #print "---------- -------- -------- -------- -------- -------- -------- --------------- ---------------"
            print "%9.6f %8d %8d %8d %8d %8d %8d %15.13f %15.13f" % (Xa,(tp+fp),tp,fp,(tn+fn),tn,fn,accObj[accstat][0],accObj[accstat][1])
    return accObj[accstat]

def getROCCurve(xMin,width,hist0,hist1,verbose=False):
    print ""
    print "Generating ROC Curve"
    nSteps = max(len(hist0),len(hist1))
    ROCx = []
    ROCy = []
    if (verbose):
        print "        Xa    predp       tp       fp    predn       tn       fn           stat1           stat2"
        print "---------- -------- -------- -------- -------- -------- -------- --------------- ---------------"
    for i in range(nSteps+1):
        Xa = xMin + width*i
        ROC = getAccuracyFromSingleThreshold(xMin,width,Xa,hist0,hist1,'ROC_Pair',verbose)
        ROCx.append(ROC[0])
        ROCy.append(ROC[1])
    print "Plotting..."
    plt.plot(ROCx,ROCy)
    plt.xlabel('False Positive Rate (1 - Specificity)')
    plt.ylabel('True Positive Rate (Sensitivity)')
    plt.title('ROC Curve')
    plt.show()
    return {'x':ROCx,'y':ROCy}

def getAccuracyFromDualThreshold(xMin,width,Xa,Xb,hist0,hist1,accstat,verbose=True):
    Na = int((Xa-xMin)/width)
    Nb = int((Xb-xMin)/width)
    fp = sum(hist0[Na:Nb])
    tp = sum(hist1[Na:Nb])
    fn = sum(hist1[:Na]) + sum(hist1[Nb:])
    tn = sum(hist0[:Na]) + sum(hist0[Nb:])
    accObj = getAccuracy(tp,fp,tn,fn)
    if (verbose):
        print "%8.6f %8.6f %8d %8d %8d %8d %8d %8d %15.13f" % (Xa,Xb,(tp+fp),tp,fp,(tn+fn),tn,fn,accObj[accstat])
    return accObj[accstat]

def XaNudge(xMin,width,Xa,Xb,hist0,hist1,dx,accstat,verbose=True):
    if (verbose):
        print "       Xa       Xb    predp       tp       fp    predn       tn       fn            stat"
        print "--------- -------- -------- -------- -------- -------- -------- -------- ---------------"
    accdn = getAccuracyFromDualThreshold(xMin,width,Xa-dx,Xb,hist0,hist1,accstat,verbose)
    acc0 = getAccuracyFromDualThreshold(xMin,width,Xa,Xb,hist0,hist1,accstat,verbose)
    accup = getAccuracyFromDualThreshold(xMin,width,Xa+dx,Xb,hist0,hist1,accstat,verbose)
    xup = accup-acc0
    if xup != 0:
        yup = max(xup,0)/xup
    else:
        yup = 0
    xdn = accdn-acc0
    if xdn != 0:
        ydn = max(xdn,0)/xdn
    else:
        ydn = 0
    return Xa + dx*yup - dx*ydn
    
def XbNudge(xMin,width,Xa,Xb,hist0,hist1,dx,accstat,verbose=True):
    accdn = getAccuracyFromDualThreshold(xMin,width,Xa,Xb-dx,hist0,hist1,accstat,verbose)
    acc0 = getAccuracyFromDualThreshold(xMin,width,Xa,Xb,hist0,hist1,accstat,verbose)
    accup = getAccuracyFromDualThreshold(xMin,width,Xa,Xb+dx,hist0,hist1,accstat,verbose)
    if (verbose):
        print "--------------------------"
    xup = accup-acc0
    if xup != 0:
        yup = max(xup,0)/xup
    else:
        yup = 0
    xdn = accdn-acc0
    if xdn != 0:
        ydn = max(xdn,0)/xdn
    else:
        ydn = 0
    return Xb + dx*yup - dx*ydn

def optimizeXaXb(imgXfn,imgTruthfn,startXa,startXb,maxIter,accstat,verbose=True):
    print ""
    print "Optimizing Dual Thresholds by Comparing " + imgXfn + " with " + imgTruthfn
    print "stat: " + accstat
    imgX = ee.Image(imgXfn)
    imgTruth = ee.Image(imgTruthfn) 
    imgX1 = ee.Image(imgX).mask(ee.Image(imgTruth).eq(1))
    imgX0 = ee.Image(imgX).mask(ee.Image(imgTruth).eq(0))
    histAll = getHistogram(imgX)
    xMin = histAll.get("bucketMin").getInfo()
    histogram = histAll.get("histogram").getInfo()
    steps = len(histAll.get("histogram").getInfo())
    width = histAll.get("bucketWidth").getInfo()
    xMax = xMin + width*steps
    hist1 = getFixedHistogram(imgX1,xMin,xMax,steps)
    hist0 = getFixedHistogram(imgX0,xMin,xMax,steps)
    Xa = startXa
    Xb = startXb
    iter = 0
    success = 0
    while True:
        iter = iter + 1
        Xa_new = XaNudge(xMin,width,Xa,Xb,hist0,hist1,width,accstat,verbose)
        Xb_new = XbNudge(xMin,width,Xa_new,Xb,hist0,hist1,width,accstat,verbose)
        if ((Xa_new == Xa) and (Xb_new == Xb)):
            success = 1
            print "%8.6f %8.6f" % (Xa,Xb) + " - Optimization achieved using " + accstat
            print "--------------------------"
            break
        if (iter >= maxIter):
            print "Maximum number of iterations reached."
            break
        else:
            Xa = Xa_new
            Xb = Xb_new
            print "%8.6f %8.6f" % (Xa,Xb)
            if (verbose):
                print "--------------------------"
    return [Xa_new,Xb_new,success]
